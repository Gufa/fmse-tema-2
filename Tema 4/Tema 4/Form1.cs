﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;

namespace Tema_4
{

    public partial class Form1 : Form
    {

     

        MySql.Data.MySqlClient.MySqlConnection conn;
        MySqlCommand command;
        public string nnume;
        TreeNode ulimulNodSelectat = new TreeNode();

        public Form1()
        {
            InitializeComponent();
            listBox1.Items.Clear();
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            string myConnectionString = "server=server.gplay.ro;uid=mihaela;pwd=mihaela55.;database=mihaela";
            conn.ConnectionString = myConnectionString;
            conn.Open();
            command = new MySqlCommand("", conn);

        }


        static void dirSearch(string dir)
        {
            try
            {
                Console.WriteLine("fisiere din: " + dir + "\n");
                string[] words = dir.Split('\\');
                string parent;
                string query;

                parent = words[words.Length - 1];
                if (words.Length - 1 == 1)
                {
                    query = "INSERT INTO tree (name,parent,type) VALUES('" + words[0] + "','null','partitie')";
                    inserare(query);
                    query = "INSERT INTO tree (name,parent,type) VALUES('" + words[1] + "','" + words[0] + "','director')";
                    inserare(query);
                }

                foreach (string f in Directory.GetFiles(dir))
                {
                    words = f.Split('\\');
                    string fisier = words[words.Length - 1];
                    query = "INSERT INTO tree (name,parent,type) VALUES('" + fisier + "','" + parent + "','fisier')";
                    inserare(query);
                    Console.WriteLine(f);
                }
                foreach (string d in Directory.GetDirectories(dir))
                {
                    Console.WriteLine("DIRECTOR:" + d);
                    words = d.Split('\\');
                    string director = words[words.Length - 1];
                    parent = words[words.Length - 2];
                    query = "INSERT INTO tree (name,parent,type) VALUES('" + director + "','" + parent + "','director')";
                    inserare(query);
                    dirSearch(d);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }


        }

        static void inserare(string query)
        {
            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
            string myConnectionString = "server=server.gplay.ro;uid=mihaela;pwd=mihaela55.;database=mihaela";
            conn.ConnectionString = myConnectionString;
            try
            {

                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);

            }

        }


        private void selectFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string folderPath = "";
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog1.SelectedPath;

            }

            backgroundWorker1.RunWorkerAsync(folderPath);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            dirSearch(e.Argument.ToString());

        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Datele au fost introduse cu succes in baza de date!");
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
            string myConnectionString = "server=server.gplay.ro;uid=mihaela;pwd=mihaela55.;database=mihaela";
            conn.ConnectionString = myConnectionString;
            try
            {
                conn.Open();
                treeView1.BeginUpdate();

                string getRootFolderQuery = "SELECT name FROM tree WHERE parent='null'";
                command = new MySqlCommand("", conn);
                command.CommandText = getRootFolderQuery;
                string root = command.ExecuteScalar() as string;

                TreeNode t = treeView1.Nodes.Add(root);
                create_Tree(t, conn);
                treeView1.EndUpdate();



            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);

            }
            conn.Close();
        }

        void create_Tree(TreeNode node, MySql.Data.MySqlClient.MySqlConnection conn)
        {

            string query = "select * from tree where parent='" + node.Text + "'";
            command = new MySqlCommand(query, conn);
            MySqlDataReader datareader = command.ExecuteReader();

            while (datareader.Read())
            {
                TreeNode n = new TreeNode(datareader["name"] + " ");
                node.Nodes.Add(n);
            }

            datareader.Close();

            foreach (TreeNode n in node.Nodes)
                create_Tree(n, conn);


            if (datareader != null)
            {
                datareader.Close();
                datareader.Dispose();
            }



        }


        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {


            TreeNode node = e.Node;
            treeView1.SelectedNode = node;
            string query = "SELECT * FROM tree WHERE parent='" + node.Text + "'";
            listBox1.Items.Clear();

            conn = new MySql.Data.MySqlClient.MySqlConnection();
            string myConnectionString = "server=server.gplay.ro;uid=mihaela;pwd=mihaela55.;database=mihaela";
            conn.ConnectionString = myConnectionString;
            conn.Open();
            command = new MySqlCommand(query, conn);
            MySqlDataReader datareader = command.ExecuteReader();

            while (datareader.Read())
            {
                string name = datareader["name"] + " ";
                listBox1.Items.Add(name);
            }


            if (datareader != null)
            {
                datareader.Close();
                datareader.Dispose();
            }

        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            treeView1.BeforeExpand -= treeView1_BeforeExpand;
            if (e.Node.Level == 1)
            {
                e.Node.ExpandAll();
            }
            treeView1.BeforeExpand += treeView1_BeforeExpand;
        }

        int optiune = 0;

        private void adaugareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Show();
            button1.Show();
            optiune = 1;
        }

        string itemselectat;
        bool selected = false;

        private void button1_Click(object sender, EventArgs e)
        {
            conn.Open();
            if (optiune == 1) //adaugare
            {
                string name = textBox1.Text;
                string parent = treeView1.SelectedNode.Text;

                if (name != " " && name != null)
                {

                    string query = "INSERT INTO tree(name,parent,type) VALUES('" + name + "','" + parent + "','fisier')";
                    command = new MySqlCommand(query, conn);
                    int randuri = command.ExecuteNonQuery();
                    listBox1.Items.Add(name);
                    TreeNode node = new TreeNode(name);
                    treeView1.SelectedNode.Nodes.Add(node);


                }
            } //modificare
            else if (optiune == 2)
            {
                if (selected == true)
                {
                    string name = itemselectat;
                    string newname = textBox1.Text;
                    string oldname = name;
                    string query = "UPDATE tree SET name='" + newname + "' WHERE name='" + oldname + "'";
                    command = new MySqlCommand(query, conn);
                    int randuri = command.ExecuteNonQuery();
                    MessageBox.Show("Au fost modificate " + randuri + " randuri.");

                    listBox1.SelectedItem = newname;
                    listBox1.Refresh();
                }
                else
                {
                    MessageBox.Show("Nu ati selectat nimic!");
                }
            }

            textBox1.Hide();
            button1.Hide();
        }

        private void stergereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            conn.Open();
            if (listBox1.Focused)
            {
                string name = listBox1.SelectedItem.ToString();
                if (search(ulimulNodSelectat, name)) //foldere
                {
                    string query = "DELETE FROM tree WHERE name='" + name + "'";
                    command = new MySqlCommand(query, conn);
                    int randuri_sterse = command.ExecuteNonQuery();

                    query = "DELETE FROM tree WHERE parent='" + name + "'";
                    command = new MySqlCommand(query, conn);
                    randuri_sterse += command.ExecuteNonQuery();
                    MessageBox.Show(randuri_sterse + "randuri sterse.");

                    listBox1.Items.Clear();
                    treeView1.Nodes.Remove(ulimulNodSelectat);
                }
                else //fisier
                {
                    string query = "DELETE FROM tree WHERE name='" + name + "'";
                    command = new MySqlCommand(query, conn);
                    int randuri_sterse = command.ExecuteNonQuery();
                    MessageBox.Show(randuri_sterse + "randuri sterse.");

                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                }
            }
            else if (treeView1.Focused)
            {
                string name = treeView1.SelectedNode.Text;

                string query = "DELETE FROM tree WHERE name='" + name + "'";
                command = new MySqlCommand(query, conn);
                int randuri_sterse = command.ExecuteNonQuery();

                query = "DELETE FROM tree WHERE parent='" + name + "'";
                command = new MySqlCommand(query, conn);
                randuri_sterse += command.ExecuteNonQuery();
                MessageBox.Show(randuri_sterse + "randuri sterse.");

                listBox1.Items.Clear();
                treeView1.Nodes.Remove(treeView1.SelectedNode);
            }
            conn.Close();
        }

        private bool search(TreeNode node, string name)
        {
            foreach (TreeNode n in node.Nodes)
                if (n.Text == name) return true;
            return false;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ulimulNodSelectat = treeView1.SelectedNode;
        }

        private void modificareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Show();
            button1.Show();
            optiune = 2;
            if (listBox1.SelectedItem.ToString() != null) itemselectat = listBox1.SelectedItem.ToString();
            selected = true;
        }
    }
}
